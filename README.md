# Guía de configuración básica de servidor Linux basado en Ubuntu Server LTS

Este es el código de la guía, esta en escrito en DocBook XML para permitir abstraer el contenido de la presentación, se puede convertir el documento a formato HTML para publicarlo en línea.

## Requerimientos

Para validar la sintaxis del archivo Dockbook XML y convertirlos a HTML se requieren los siguientes paquetes:

 * make
 * libxml2-utils
 * xsltproc

Para convertir de DocBook XML se requieren los siguientes paquetes:

 * xmlto

## Validando la documentación

Si haces cambios asegurate de validar la sintaxis usando:

```shell
$ make test
```

## Convirtiendo a formato HTML

```shell
$ make build
```

El documento en formato HTML se almacena en el directorio html.

## Limpiando el espacio de trabajo

Para limpiar los archivos html e imagenes generados, ejecute el target clean.

```shell
$ make build
```

